<?php

namespace App\Http\Controllers\Admin\EmergencyAssistance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\EmergencyContacts\EmergencyAssistance;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class EmergencyAssistanceController extends Controller
{
    public function index()
    {
        $emergencyAssistances = EmergencyAssistance::orderBy('created_at' , 'desc')->get();
        return view('admin.emergencyAssistance.index', compact('emergencyAssistances'));
    }


    public function create()
    {
        return view('admin.emergencyAssistance.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'emergency_assistance_name' => 'required|max:255',
            'contact_number' => 'required|numeric',
        ]);

        try{
            $EmergencyAssistance = new EmergencyAssistance;
            $EmergencyAssistance->name = $request->emergency_assistance_name;
            $EmergencyAssistance->contact_no = $request->contact_number;
            $EmergencyAssistance->save();
            return redirect()->route('admin.emergency-assistance.index')->with('flash_success','Emergency Assistance Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Emergency Assistance Not Found');
        }

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try {
            $preference = EmergencyAssistance::findOrFail($id);
            return view('admin.emergencyAssistance.edit',compact('preference'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'emergency_assistance_name' => 'required|max:255',
            'contact_number' => 'required|numeric',
        ]);

        try {
            EmergencyAssistance::where('emergency_assistance_id',$id)->update([
                    'name' => $request->emergency_assistance_name,
                    'contact_no' => $request->contact_number,
                ]);
            return redirect()->route('admin.emergency-assistance.index')->with('flash_success', 'Emergency Assistance Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Emergency Assistance Not Found');
        }
    }

   	public function destroy($id)
    {
        try {
            EmergencyAssistance::find($id)->delete();
            return back()->with('flash_success', 'Emergency assistance deleted successfully');
        } 
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Emergency Assistance Not Found');
        }
    }

}
