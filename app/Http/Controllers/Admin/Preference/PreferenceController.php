<?php

namespace App\Http\Controllers\Admin\Preference;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Preference\Preference;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Model\DriverPreference\DriverPreference;

class PreferenceController extends Controller
{
    public function index()
    {
        $preferences = Preference::orderBy('created_at' , 'desc')->get();
        return view('admin.preference.index', compact('preferences'));
    }

    public function driverPreferenceIndex($id){
        return view('admin.driver.driver-preference', compact('id'));
    }

    public function driverPreference(Request $request){
        $id=$request->driver_id;
        $preferences = Preference::orderBy('created_at' , 'desc')->get();
        foreach($preferences as $value){
            if(count($value->driver_preference)>0){
                foreach($value->driver_preference as $driver_prefer){
                    if($driver_prefer->user_id==$id){
                        $value->driver_prefer_id = $driver_prefer->preference_id;
                        $value->checked = $driver_prefer->checked;
                    }
                }
            }
        }
        return response()->json([
            'success' => true,
            'message' => 'Driver Preference.',
            'data' => $preferences,
        ], 200);
    }

    public function updateDriverPreference(Request $request){
        $driver_id = $request->driver_id;
        $preference_id = $request->preference_id;
        $isChecked = $request->isChecked;
        $driverPreference = DriverPreference::where('user_id',$driver_id)->where('preference_id',$preference_id)->first();
        if(isset($driverPreference)){
            if($isChecked=='YES'){
                $driverPreference->preference_id = $preference_id;
                $driverPreference->user_id = $driver_id;
                $driverPreference->checked = 1;
                $driverPreference->save();
            }
            if($isChecked=='NO'){
                $driverPreference->preference_id = $preference_id;
                $driverPreference->user_id = $driver_id;
                $driverPreference->checked = 0;
                $driverPreference->save();
            }
        } else {
            if($isChecked=='YES'){
                $driverPreference = new DriverPreference;
                $driverPreference->preference_id = $preference_id;
                $driverPreference->user_id = $driver_id;
                $driverPreference->checked = 1;
                $driverPreference->save();
            }
            if($isChecked=='NO'){
                $driverPreference = new DriverPreference;
                $driverPreference->preference_id = $preference_id;
                $driverPreference->user_id = $driver_id;
                $driverPreference->checked = 0;
                $driverPreference->save();
            }
        }
        return response()->json([
            'success' => true,
            'message' => 'Update Driver Preference.',
            'data' => $driverPreference,
        ], 200);
    }


    public function create()
    {
        return view('admin.preference.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'preference_name' => 'required|max:255',
        ]);

        try{
            $preference = new Preference;
            $preference->preference_name = $request->preference_name;
            $preference->save();
            return redirect()->route('admin.preference.index')->with('flash_success','Preference Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Preference Not Found');
        }

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try {
            $preference = Preference::findOrFail($id);
            return view('admin.preference.edit',compact('preference'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'preference_name' => 'required|max:255',
        ]);

        try {
            Preference::where('preference_id',$id)->update([
                    'preference_name' => $request->preference_name,
                ]);
            return redirect()->route('admin.preference.index')->with('flash_success', 'Preference Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Preference Not Found');
        }
    }

}
