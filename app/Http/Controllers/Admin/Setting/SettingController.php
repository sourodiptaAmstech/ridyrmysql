<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ->orWhere('key','web_site')->orWhere('key','contact_number')->orWhere('key','contact_email')->orWhere('key','sos_number')
        $site_settings = Setting::where('key','site_title')->orWhere('key','site_logo')->orWhere('key','site_email_logo')->orWhere('key','site_icon')->orWhere('key','site_copyright')->orWhere('key','female_friendly')->orWhere('key','commission')->orWhere('key','passenger_cancellation_charge')->orWhere('key','driver_cancellation_charge')->orWhere('key','contact_number')->orWhere('key','contact_email')->get();

        return view('admin.setting.index', compact('site_settings'));
    }

    public function femaleFriendlyUpdate(Request $request)
    {
        if ($request->status=='YES'){
            $setting = Setting::where('key','female_friendly')->first();
            $setting->value = 'Y';
            $setting->save();
        }
        if ($request->status=='NO'){
            $setting = Setting::where('key','female_friendly')->first();
            $setting->value = 'N';
            $setting->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Updated Successfully.',
            'data' => $setting,
        ], 200);
    }

    public function getFemaleFriendly(Request $request)
    {
        $setting = Setting::where('key','female_friendly')->first();
        return response()->json([
            'success' => true,
            'message' => 'Get female friendly.',
            'data' => $setting,
        ], 200);
    }
    
    public function privacyPolicy()
    {
        $policies = Setting::where('key','condition_privacy')
                    ->orWhere('key','page_privacy')
                    ->orWhere('key','condition_privacy_driver')
                    ->get();
        return view('admin.setting.policy', compact('policies'));
    }

    public function privacyPolicyUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'condition_privacy'||$site_setting->key == 'page_privacy'||$site_setting->key == 'condition_privacy_driver') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.privacy.policy')->with('flash_success', 'Privacy Policy Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Privacy Policy Not Found');
        }
    }


    public function privacyPolicyFrench()
    {
        $policies = Setting::where('key','condition_privacy_fr')
                    ->orWhere('key','page_privacy_fr')
                    ->orWhere('key','condition_privacy_driver_fr')
                    ->get();
        return view('admin.setting.policyfr', compact('policies'));
    }

    public function privacyPolicyUpdateFrench(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'condition_privacy_fr'||$site_setting->key == 'page_privacy_fr'||$site_setting->key == 'condition_privacy_driver_fr') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.privacy.policy.french')->with('flash_success', 'Privacy Policy Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Privacy Policy Not Found');
        }
    }

    //Home Content
    public function homeContent()
    {
        $contents = Setting::where('key','select_your_ride')
                    ->orWhere('key','select_your_destination')
                    ->orWhere('key','card_payment')
                    ->orWhere('key','entro_ride_sharing')
                    ->orWhere('key','ride_sharing_analytics')
                    ->orWhere('key','entro_ride_form_app')
                    ->get();
        return view('admin.setting.content', compact('contents'));
    }

    public function homeContentUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'select_your_ride'||$site_setting->key == 'select_your_destination'||$site_setting->key == 'card_payment'||$site_setting->key == 'entro_ride_sharing'||$site_setting->key == 'ride_sharing_analytics'||$site_setting->key == 'entro_ride_form_app') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.home.content')->with('flash_success', 'Content Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Content Not Found');
        }
    }


    public function homeContentFrench()
    {
        $contents = Setting::where('key','select_your_ride_fr')
                    ->orWhere('key','select_your_destination_fr')
                    ->orWhere('key','card_payment_fr')
                    ->orWhere('key','entro_ride_sharing_fr')
                    ->orWhere('key','ride_sharing_analytics_fr')
                    ->orWhere('key','entro_ride_form_app_fr')
                    ->get();
        return view('admin.setting.contentfr', compact('contents'));
    }

    public function homeContentUpdateFrench(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'select_your_ride_fr'||$site_setting->key == 'select_your_destination_fr'||$site_setting->key == 'card_payment_fr'||$site_setting->key == 'entro_ride_sharing_fr'||$site_setting->key == 'ride_sharing_analytics_fr'||$site_setting->key == 'entro_ride_form_app_fr') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.home.content.french')->with('flash_success', 'Content Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Content Not Found');
        }
    }


    public function getSafetyTools()
    {
        $safetys = Setting::where('key','learn_about_safety')
                    ->orWhere('key','system_tool')
                    ->get();
        return view('admin.setting.safety', compact('safetys'));
    }

    public function updateSafetyTools(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'learn_about_safety'||$site_setting->key == 'system_tool') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.safety.tools')->with('flash_success', 'Safety Tools Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Safety Tools Not Found');
        }
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            return view('admin.setting.edit',compact('site_setting'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            if ($request->key == 'site_title'||$request->key == 'site_copyright'||$site_setting->key == 'web_site'||$site_setting->key == 'contact_number'||$site_setting->key == 'contact_email'||$site_setting->key == 'sos_number'||$site_setting->key == 'commission'||$site_setting->key == 'passenger_cancellation_charge'||$site_setting->key == 'driver_cancellation_charge') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            } else {
                $this->validate($request, [
                    'value' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                ]);
                if(isset($request->value) && !empty($request->value)){
                    //$Storage=Storage::delete($site_setting->value);
                    $value = $request->value->store('public/setting/'.$id);
                    $value=str_replace("public", "storage", $value);
                    $site_setting->value=$value;
                }
            }
            $site_setting->save();
            return redirect()->route('admin.setting.index')->with('flash_success', 'Site Setting Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
