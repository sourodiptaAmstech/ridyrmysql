<?php
namespace App\Http\Controllers\Api\Background;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document\DriverDocuments;
use App\Model\Document\Document;
use App\Model\Device\UserDevices;
use App\Services\DriversProfileService;
use App\Services\RequestServices;
use App\Services\UsersDevices;
use App\Services\ServiceRequestService;
use App\Services\PassengersProfileService;
use App\Services\EstimatedFareService;
use App\Services\LocationService;
use App\Model\Request\RequestPreferences;

use Validator;

class DriversBackgroundController extends Controller
{
    public function DocumentCompletion(){
        try{
            $total=0;
            $profileFillUp=0;

            $user_id=Auth::user()->id;
            $DriverDocument=DriverDocuments::where("user_id",$user_id)->get()->toArray();
            $profileFillUp+=count($DriverDocument);

            $Document=Document::where("deleted_at",null)->where("status",1)->get()->toArray();
            $total+=count($Document);
            $percentage=($profileFillUp*100)/$total;

            return $percentage;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return 0;
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return 0;
        }
        catch(ModelNotFoundException $e)
        {
            return 0;
        }
    }
    private function requestedRide($profileData){
        // check the service request log and get the ride details
        $RequestServices=new RequestServices();
        $EstimatedFareService =new EstimatedFareService();
        $LocationService=new LocationService();
        $RequestServicesReturn=$RequestServices->accessGetSerReqLogBYDriver((object)['driver_id'=>$profileData->user_id,"service_type_id"=>$profileData->active_service_id,"status"=>"REQUESTED"]);
        $requestData=[];
        if(!empty($RequestServicesReturn)){
            // get request details and passanger details
            $ServiceRequestService =new ServiceRequestService();
            $PassengersProfileService=new PassengersProfileService();
            $UsersDevices= new UsersDevices();

            $ServiceRequestServiceReturn = $ServiceRequestService->accessGetRequestByID((object)$RequestServicesReturn[0]);

            $PassengersProfileServiceReturn=$PassengersProfileService->accessGetProfile((object)['user_id'=>$RequestServicesReturn[0]['passenger_id']]);

            $UsersDevicesReturn=$UsersDevices->accessGetDevice((object)['user_id'=>$RequestServicesReturn[0]['passenger_id']]);

            $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

            $locDetails=$LocationService->accessGetLocation($ServiceRequestServiceReturn->request_id);
            $nextDropLocation=["status"=>"COMPLETED","location"=>(object)[],'isLocation'=>"NONE"];
            foreach($locDetails as $locKey =>$locVal){
                switch($locVal['types']){
                    case "source":
                        $locDetailsArray["source"]=     (object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],"location_id"=>$locVal['location_id'],"reached_on"=>$locVal['reached_on'],"started_on"=>$locVal['started_on']];
                    break;
                    case "destination":
                        $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],"location_id"=>$locVal['location_id'],"reached_on"=>$locVal['reached_on'],"started_on"=>$locVal['started_on']];
                    break;
                    case "waypoint":
                        $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],"location_id"=>$locVal['location_id'],"reached_on"=>$locVal['reached_on'],"started_on"=>$locVal['started_on']];
                    break;
                }
            }
            if($nextDropLocation['status']=="COMPLETED"){
                if($locDetailsArray["source"]->reached_on==null or $locDetailsArray["source"]->started_on==null){
                    $nextDropLocation=["status"=>"PENDING","isLocation"=>"SOURCE","location"=>$locDetailsArray["source"]];
                }
            }
            if($nextDropLocation['status']=="COMPLETED"){
             if(count($locDetailsArray['waypoints'])>0){
                foreach($locDetailsArray['waypoints'] as $key=>$valKeys){
                    if($valKeys->orders==1 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==2 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==3 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==4 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==5 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                }
            }
        }

            if($nextDropLocation['status']=="COMPLETED"){
                if($locDetailsArray["destination"]->reached_on==null){
                    $nextDropLocation=["status"=>"PENDING","isLocation"=>"DESTINATION","location"=>$locDetailsArray["destination"]];
                }
            }
            $childSafetyFeature="No";
            $RequestPreferences=RequestPreferences::where("request_id",$ServiceRequestServiceReturn->request_id)->where("preference_id",8)->get()->toArray();
            if(count($RequestPreferences)>0){
                if($ServiceRequestServiceReturn->isVerifyChildSafty==0){
                    $childSafetyFeature="Yes";
                }
            }
            $requestData=[
                'passenger_firstName'=>$PassengersProfileServiceReturn['data']->first_name,
                "passenger_lastName"=>$PassengersProfileServiceReturn['data']->last_name,
                "passenger_img"=>$PassengersProfileServiceReturn['data']->picture,
                "passenger_mobile"=>$PassengersProfileServiceReturn['data']->mobile_no,
                "passenger_isdcode"=>$PassengersProfileServiceReturn['data']->isd_code,
                "passenger_overall_rating"=>$PassengersProfileServiceReturn['data']->overall_rating,
                "passenger_location"=>["latitude"=>(float)$UsersDevicesReturn->latitude,"longitude"=>(float)$UsersDevicesReturn->longitude],
                // "passenger_prefrence"=>"",
                "location_details"=>(array)$locDetailsArray,
                "estimatedFare"=>$EstimatedFareService->accessGetEstimated((object)["request_id"=>$ServiceRequestServiceReturn->request_id]),
                "request_no"=>$ServiceRequestServiceReturn->request_no,
                "request_id"=>$ServiceRequestServiceReturn->request_id,
                "request_type"=>$ServiceRequestServiceReturn->request_type,
                "request_status"=>$ServiceRequestServiceReturn->request_status,
                "payment_method"=>$ServiceRequestServiceReturn->payment_method,
                "staredFromSource_on"=>$ServiceRequestServiceReturn->started_from_source,
                "dropped_on_destination"=>$ServiceRequestServiceReturn->dropped_on_destination,
                "nextDropLocation"=>$nextDropLocation,
                "driver_rating_status"=>$ServiceRequestServiceReturn->driver_rating_status,
                "childSafetyFeature"=>$childSafetyFeature
            ];
        }
        return $requestData;
    }

    private function getServiceRequestDetails($profileData){
        // check the service request log and get the ride details
        $RequestServices=new RequestServices();
        $EstimatedFareService =new EstimatedFareService();
        $LocationService=new LocationService();
        $RequestServicesReturn=$RequestServices->accessGetSerReqLogBYDriver((object)['driver_id'=>$profileData->user_id,"service_type_id"=>$profileData->active_service_id,"status"=>"ONRIDE"]);

        $requestData=[];
        if(!empty($RequestServicesReturn)){
            // get request details and passanger details
            $ServiceRequestService =new ServiceRequestService();
            $PassengersProfileService=new PassengersProfileService();
            $UsersDevices= new UsersDevices();

            $ServiceRequestServiceReturn = $ServiceRequestService->accessGetRequestByID((object)$RequestServicesReturn[0]);

            $PassengersProfileServiceReturn=$PassengersProfileService->accessGetProfile((object)['user_id'=>$RequestServicesReturn[0]['passenger_id']]);

            $UsersDevicesReturn=$UsersDevices->accessGetDevice((object)['user_id'=>$RequestServicesReturn[0]['passenger_id']]);
            $DriverDevice=$UsersDevices->accessGetDevice((object)['user_id'=>$profileData->user_id]);

            $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

            $locDetails=$LocationService->accessGetLocation($ServiceRequestServiceReturn->request_id);

            $nextDropLocation=["status"=>"COMPLETED","location"=>(object)[],"isLocation"=>"NONE"];

            foreach($locDetails as $locKey =>$locVal){

                switch($locVal['types']){
                    case "source":
                        $locDetailsArray["source"]=     (object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],"location_id"=>$locVal['location_id'],"reached_on"=>$locVal['reached_on'],"started_on"=>$locVal['started_on']];
                    break;
                    case "destination":
                        $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],"location_id"=>$locVal['location_id'],"reached_on"=>$locVal['reached_on'],"started_on"=>$locVal['started_on']];
                    break;
                    case "waypoint":
                        $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],"location_id"=>$locVal['location_id'],"reached_on"=>$locVal['reached_on'],"started_on"=>$locVal['started_on']];
                    break;
                }
            }
            if($nextDropLocation['status']=="COMPLETED"){
                if($locDetailsArray["source"]->reached_on==null or $locDetailsArray["source"]->started_on==null){
                    $nextDropLocation=["status"=>"PENDING","isLocation"=>"SOURCE","location"=>$locDetailsArray["source"]];
                }
            }
            if($nextDropLocation['status']=="COMPLETED"){
             if(count($locDetailsArray['waypoints'])>0){
                foreach($locDetailsArray['waypoints'] as $key=>$valKeys){
                    if($valKeys->orders==1 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==2 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==3 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==4 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                    else if($valKeys->orders==5 && ($valKeys->reached_on==null or $valKeys->started_on==null)){
                        $nextDropLocation=["status"=>"PENDING","isLocation"=>"WAYPOINTS","location"=>$valKeys];
                        break;
                    }
                }
            }
        }


            if($nextDropLocation['status']=="COMPLETED"){
                if($locDetailsArray["destination"]->reached_on==null){
                    $nextDropLocation=["status"=>"PENDING","isLocation"=>"DESTINATION","location"=>$locDetailsArray["destination"]];
                }
            }
            $childSafetyFeature="No";
            $RequestPreferences=RequestPreferences::where("request_id",$ServiceRequestServiceReturn->request_id)->where("preference_id",8)->get()->toArray();
            if(count($RequestPreferences)>0){
                if($ServiceRequestServiceReturn->isVerifyChildSafty==0){
                    $childSafetyFeature="Yes";
                }
            }

            $requestData=[
                'passenger_firstName'=>$PassengersProfileServiceReturn['data']->first_name,
                "passenger_lastName"=>$PassengersProfileServiceReturn['data']->last_name,
                "passenger_img"=>$PassengersProfileServiceReturn['data']->picture,
                "passenger_mobile"=>$PassengersProfileServiceReturn['data']->mobile_no,
                "passenger_isdcode"=>$PassengersProfileServiceReturn['data']->isd_code,
                "passenger_overall_rating"=>$PassengersProfileServiceReturn['data']->overall_rating,
                "passenger_location"=>["latitude"=>(float)$UsersDevicesReturn->latitude,"longitude"=>(float)$UsersDevicesReturn->longitude],
                // "passenger_prefrence"=>"",
                "location_details"=>(array)$locDetailsArray,
                // "estimatedFare"=>$EstimatedFareService->accessGetEstimated((object)["request_id"=>$ServiceRequestServiceReturn->request_id]),
                "request_no"=>$ServiceRequestServiceReturn->request_no,
                "request_id"=>$ServiceRequestServiceReturn->request_id,
                "request_type"=>$ServiceRequestServiceReturn->request_type,
                "request_status"=>$ServiceRequestServiceReturn->request_status,
                "payment_method"=>$ServiceRequestServiceReturn->payment_method,
                "staredFromSource_on"=>$ServiceRequestServiceReturn->started_from_source,
                "dropped_on_destination"=>$ServiceRequestServiceReturn->dropped_on_destination,
                "driver_rating_status"=>$ServiceRequestServiceReturn->driver_rating_status,
                "nextDropLocation"=>$nextDropLocation,
                "childSafetyFeature"=>$childSafetyFeature
               // "driver_location"=>["latitude"=>(float)$UsersDevicesReturn->latitude,"longitude"=>(float)$UsersDevicesReturn->longitude],
            ];
        }
        return $requestData;
    }

    private function calculateFinalPayable($data){
        $EstimatedFareService=new EstimatedFareService();
        return $EstimatedFareService->accessCalculateFinalPayable((object)$data);
    }



    public function get(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'device_latitude'=>'required',
                'device_longitude'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $EstimatedFareService=new EstimatedFareService();
            //update lat log
            $user_id=Auth::user()->id;
            $UserDevices = UserDevices::where('user_id',$user_id)->first();
            $UserDevices->latitude = $request->device_latitude;
            $UserDevices->longitude = $request->device_longitude;
            $UserDevices->save();

            $requestedRide=[];
            $status="NORMAL";
            $message="";

            $request->user_scope="driver-service";
            $request->user_id=Auth::user()->id;

            $DriversProfileService=new DriversProfileService();
            $driverDeatils=$DriversProfileService->accessGetProfile($request);
            $UsersDevices=new UsersDevices();

            ##### On off #######
            $onOffStatus= $driverDeatils['data']->status;
            $service_status="INACTIVE";
           // $driverDeatils['data']->service_status;


            ###### Check For request ########
        //    print_r($driverDeatils['data']);

            switch($driverDeatils['data']->service_status){
                case "ACTIVE":
                    $status="NORMAL";
                    $message="";
                    $service_status="ACTIVE";
                break;
                case "REQUESTED":
                    $requestedRide=$this->requestedRide($driverDeatils['data']);
                    $status="REQUESTED";
                    $message=trans("api.NOTIFICATION.MESSAGE.RIDE_REQUEST");
                    $service_status="ACTIVE";
                break;
                case "ONRIDE":
                    $service_status="ACTIVE";
                    // get the status of the ride from the request services
                    $requestedRide=$this->getServiceRequestDetails($driverDeatils['data']);
                    switch($requestedRide['request_status']){
                        case"ACCEPTED":
                            $status="ACCEPTED";
                            //print_r($requestedRide);
                            $est=$EstimatedFareService->calculteEstimateTimeToReache((object)["source"=>['longitude'=>$request->device_longitude,'latitude'=>$request->device_latitude],"destination"=>['longitude'=>$requestedRide['passenger_location']['longitude'],'latitude'=>$requestedRide['passenger_location']['latitude']]]);
                            $requestedRide['estimatedTimeToReached']=$est['data']->duration;
                            $requestedRide['estimatedTime']=$est;
                            $message="";
                        break;
                        case"REACHED":
                            $status="REACHED";
                            $message="";
                        break;
                        case"STARTED":
                            $status="STARTED";
                            $message="";
                        break;
                        case"DROP":
                            // get the paymnent
                            $requestedRide['payable']=$this->calculateFinalPayable($requestedRide);
                            $status="DROP";
                            $message="";
                        break;
                        case"PAYMENT":
                            // get the paymnent
                            $requestedRide['payable']=$this->calculateFinalPayable($requestedRide);
                            $status="DROP";
                            $message="";
                        break;
                        case"RATING":
                            // get the paymnent
                            $requestedRide['payable']=$this->calculateFinalPayable($requestedRide);
                            $status="PAYMENT";
                            $message="";
                            // check ratting is given or not
                            if($requestedRide['driver_rating_status']=="GIVEN"){
                                // set the service request to complete, driver profile to active and service request log to complete.
                                $ServiceRequestService =new ServiceRequestService();
                                $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$requestedRide['request_id'],
                                "request_status"=>"COMPLETED","driver_id"=>$request->user_id]);

                                $DriversProfileService=new DriversProfileService();
                                $DriversProfileService->accessSetRequestToDriver((object)["driver_id"=>$request->user_id,"driver_service_status"=>"ACTIVE"],"ONRIDE");

                                $RequestServices=new RequestServices();
                                $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$requestedRide['request_id'],"status"=>"COMPLETED","driver_id"=>$request->user_id],"ONRIDE");
                                $requestedRide=[];
                                $status="NORMAL";
                                $message=trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED_DRIVER");

                            }




                        break;
                    }
                break;
            }

            $DocumentCompletion=$this->DocumentCompletion();
            $Background=[
                "document_completion"=>$DocumentCompletion,
                "onOffStatus"=>$onOffStatus,
                "status"=>$status,
                "message"=>$message,
                "requestData"=>(object)$requestedRide,
                "sdsd"=>$request->user_id,
                "service_status"=>$service_status
            ];
            return response(['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

}
