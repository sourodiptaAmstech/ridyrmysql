<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Services\DriversProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\DriversServiceType;
use App\Services\DriverPreferencesService;
use App\Services\ServiceRequestService;
use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\NotificationServices;
use App\Services\TransactionLogService;
use App\Services\PaymentService;
use App\Services\EstimatedFareService;
use App\Services\SettingServices;
use App\Services\LocationService;
use App\Services\NotificationService;
use App\Model\Request\RequestPreferences;
use App\Model\Request\ServiceRequest;

use Validator;
use Exception;

class TripController extends Controller
{
    public function reject(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
            ];
          //  echo 1; exit;
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"active","driver_id"=>$request->user_id],"requested");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"decline","driver_id"=>$request->user_id]);
            return response(['message'=>"You have decline, a request","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    private function calculateFinalPayable($data){
        $EstimatedFareService=new EstimatedFareService();
        return $EstimatedFareService->accessCalculateFinalPayable((object)$data);
    }
    public function accpect(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "request",
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"active","driver_id"=>$request->user_id],"requested");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"decline","driver_id"=>$request->user_id]);
            return response(['message'=>"You have decline, a request","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function tripControl(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
                'tripStatus'=>'required|in:ACCEPTED,REACHED,STARTED,DROP,PAYMENT,RATING,COMPLETED',
                'payment_method'=>'required_if:tripStatus,PAYMENT|in:CARD,CASH',
                'rating'=>'required_if:tripStatus,RATING',
                'comment'=>'required_if:tripStatus,RATING|max:225',
                'location_id'=>'required_if:tripStatus ACCEPTED,REACHED,STARTED,DROP',
                'isLocation'=>'required_if:tripStatus ACCEPTED,REACHED,STARTED,DROP|in:WAYPOINTS,DESTINATION,SOURCE',
                ];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
                $request->user_id=Auth::user()->id;
                $DriversProfileService=new DriversProfileService();
                $RequestServices=new RequestServices();
                $ServiceRequestService=new ServiceRequestService();
                $LocationService = new LocationService();
                $NotificationServices =new NotificationServices();
                $message="";
                $status="";
                $nextStep="";
                $otp="";
                switch($request->tripStatus){
                    
                case "ACCEPTED":
                    $RequestPreferences=RequestPreferences::where("request_id",$request->request_id)->where("preference_id",8)->get()->toArray();
                    if(count($RequestPreferences)>0){
                        // send otp to the customer for the 
                        $otp = mt_rand(1000, 9999);
                        $ServiceRequest=ServiceRequest::where("request_id",$request->request_id)->first();
                        $ServiceRequest->child_safty_otp=$otp;
                        $ServiceRequest->save();
                    }
                    //updating the driver profile
                    $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ONRIDE","driver_id"=>$request->user_id],"REQUESTED");
                    // updating the service request log
                    $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"ONRIDE","driver_id"=>$request->user_id],"REQUESTED");
                    if($RequestServicesReturn===false){
                        return response(['message'=>"Ride request your trying to accepting is not valid any more","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],204);
                    }

                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"ACCEPTED","driver_id"=>$request->user_id]);
                    $message="Thank you for accepting the request, please proceed towards the pickup location.";

                    $LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"isLocation"=>$request->isLocation,"location_id"=>$request->location_id]);
                    $Notification=new NotificationService();
                     // push
                     $pushDate=[
                        "title"=>"Ride Accepted",
                        "text"=>"Driver has been accepted your ride request. Soon be reached in your pickup location.",
                        "body"=>"Driver has been accepted your ride request. Soon be reached in your pickup location.",
                        "type"=>"RIDEACCEPTED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        if($otp!=""){
                            $pushDate2=[
                            "title"=>"Child Safety",
                            "text"=>"Your OTP for the ride is".$otp,
                            "body"=>"Your OTP for the ride is".$otp,
                            "type"=>"CHILDSAFETY",
                            "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                            "setTo"=>'SINGLE',
                            "request_id"=>$ServiceRequestServiceReturn->request_id
                            ];
                            // send sms
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate2);
                        }
                        
                     //   
                      //  $Notification->accessCreateNotification((object)$pushDate);

                      //  $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                      //  $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email

                    $status="ACCEPTED";
                    $nextStep="REACHED";
                break;
                case "REACHED":

                      // updating the service request
                      $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                      $LocationServiceReturn= $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>$request->isLocation,"location_id"=>$request->location_id]);
                      $message="Please wait for the customer";
                      $status="REACHED";
                      $nextStep="STARTED";
                   
                      /* // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                   // $LocationServiceReturn= $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                    $message="Please wait for the customer";
                    $status="REACHED";
                    $nextStep="STARTED"; */
                     // push
                     $pushDate=[
                        "title"=>"Ride Reached",
                        "text"=>"Driver has been arrived at your pickup location.",
                        "body"=>"Driver has been arrived at your pickup location.",
                        "type"=>"RIDEREACHED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                break;
                case "STARTED":
                    //caculate waiting time;
                    $RequestPreferences=RequestPreferences::where("request_id",$request->request_id)->where("preference_id",8)->get()->toArray();
                    if(count($RequestPreferences)>0){
                        // send otp to the customer for the 
                        $ServiceRequest=ServiceRequest::where("request_id",$request->request_id)->first();
                        $otp=$ServiceRequest->child_safty_otp;
                        $isVerify=$ServiceRequest->isVerifyChildSafty;
                        if($ServiceRequest->isVerifyChildSafty==0){
                            $rule=[
                                'otp' => "required",
                            ];
                            $validator=$this->requestValidation($request->all(),$rule);
                            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
                            if($request->otp==$otp){
                                $ServiceRequest->isVerifyChildSafty=1;
                                $ServiceRequest->save();
                            }
                            else{
                                return response(['message'=>"Invalid Otp! Please provide a valid OTP.","data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); 
                            }
                        }
                    }
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"STARTED","driver_id"=>$request->user_id]);
                    $LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"types"=>$request->isLocation,"location_id"=>$request->location_id]);
                    $message="Please proceed towards the destination.";
                    $status="STARTED";
                    $nextStep="DROP";
              break;
              case "DROP":                    
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"DROP","driver_id"=>$request->user_id]);
                    $LocationServiceReturn = $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"isLocation"=>$request->isLocation,"location_id"=>$request->location_id]);
                    $status="DROP";
                    $message="You have reached your destination.";
                    switch($request->isLocation){
                          case "WAYPOINTS":
                            $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                            $nextStep="STARTED";
                            $message="You have reached your drop location. Please wait while we fetch next drop location.";
                          break;
                          case "DESTINATION":
                            if($ServiceRequestServiceReturn->payment_method=="CARD"){
                                $requestedRide=[
                                    "request_no"=>$ServiceRequestServiceReturn->request_no,
                                    "request_id"=>$ServiceRequestServiceReturn->request_id,
                                    "request_type"=>$ServiceRequestServiceReturn->request_type,
                                    "request_status"=>$ServiceRequestServiceReturn->request_status,
                                    "payment_method"=>$ServiceRequestServiceReturn->payment_method,
                                    "staredFromSource_on"=>$ServiceRequestServiceReturn->started_from_source,
                                    "dropped_on_destination"=>$ServiceRequestServiceReturn->dropped_on_destination,
                                    "driver_rating_status"=>$ServiceRequestServiceReturn->driver_rating_status,
                                    "user_cards_id"=>$ServiceRequestServiceReturn->card_id,
                                    "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                                ];
                                $reqRq = $this->calculateFinalPayable($requestedRide);
                                if((float)($reqRq['cost'])-(float)($reqRq['promo_code_value'])>0){
                                    $PaymentService = new PaymentService();
                                    $retunPayment=$PaymentService->accesssMakeCardPayment((object)$reqRq,(object)$requestedRide);
                                    //print_r($retunPayment); exit;
                                    if($retunPayment['statusCode']==200){
                                        // if payment is success update the transcation table
                                        $TransactionLogService = new TransactionLogService();
                                        $reqRq['payment_gateway_charge']=$retunPayment['data']['fees'];
                                        $reqRq['payment_gateway_transaction_id']=$retunPayment['data']['reference'];
                                        $TransactionReturn=  $TransactionLogService->updateTransactionDetails((object)$reqRq);
                                        if($TransactionReturn['statusCode']===200){
                                            $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                            "request_status"=>"PAYMENT","payment_method"=>"CARD","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                            $message="Thank you for collecting the payment";
                                          //  $status="DROP";
                                            $status="PAYMENT";
                                            $nextStep="RATING";
                                        }
                                    }
                                    else{
                                        // handle paymanet failed
                                    }
                                }
                                else{
                                     // if payment is success update the transcation table
                                     $TransactionLogService = new TransactionLogService();
                                     $reqRq['payment_gateway_charge']=0;
                                     $reqRq['payment_gateway_transaction_id']=$reqRq['promo_code'];
                                     $TransactionReturn=  $TransactionLogService->updateTransactionDetails((object)$reqRq);
                                     if($TransactionReturn['statusCode']===200){
                                         $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                         "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                         $message="Thank you for collecting the payment";
                                         $status="PAYMENT";
                                         $nextStep="RATING";
                                     }
                                }
                            }
                            else{
                                $message="Please wait a while, as we calculate the payable fare";
                                $status="DROP";
                                $nextStep="PAYMENT";
                            }
                          break;
                        }

                        // push
                       /* $pushDate=[
                            "title"=>"Consignment Drop",
                            "text"=>"Your consignment has been successfully delivered.",
                            "body"=>"Your consignment has been successfully delivered.",
                            "type"=>"RIDEDROP",
                            "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                            "setTo"=>'SINGLE',
                            "request_id"=>$ServiceRequestServiceReturn->request_id,
                            'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/ride'
                        ];
                        */
                       // $Notification=new NotificationService();
                       // $Notification->accessCreateNotification((object)$pushDate);

                       // $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                       // $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                    // $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"DROP","driver_id"=>$request->user_id]);
                    //$LocationServiceReturn = $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"destination"]);
                    
                    //   print_r( $ServiceRequestServiceReturn ); exit;
                  
                break;
                case "PAYMENT":
                    if($request->payment_method=="CASH"){
                      // update the trascation log table as payament completed,
                      // update the service request table to PAYMENT and mark payment completed,
                      $TransactionLogService = new TransactionLogService();
                      $TransactionLogService->updateCreditNoteStatusForRide((object)["request_id"=>$request->request_id,"status"=>"COMPLETED"]);
                      $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                      "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                      $message="Thank you for collecting the payment";
                      $status="PAYMENT";
                      $nextStep="RATING";
                    }
                   // else if()

                break;
                case "RATING":
                    // update the service request with rating comment

                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                    "request_status"=>"RATING","rating_by_driver"=>$request->rating,"comment_by_driver"=>$request->comment,
                    "driver_rating_status"=>"GIVEN","driver_id"=>$request->user_id]);
                    $message="Thank you, for rating the customer.";
                    $status="RATING";
                    $nextStep="COMPLETED";
                     // push
                     $pushDate=[
                        "title"=>"Ride Completed",
                        "text"=>"Thank you for choosing us.",
                        "body"=>"Thank you for choosing us.",
                        "type"=>"RIDECOMPLETED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                    ];
                    $NotificationServices->sendPushNotification((object)$pushDate);
                    // send sms
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    // email
                break;
                case "COMPLETED":
                    $message="Thank you for completing the ride safely. ";
                    $status="COMPLETED";
                    $nextStep="NORMAL";
                break;
            }
            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function cancelRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];


            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get request details,
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $DriverProfile=new DriversProfileService();
            $SettingServices=new SettingServices();
            $TransactionLogService=new TransactionLogService();
            $NotificationServices =new NotificationServices();

            $getRequestByID=$ServiceRequestService->accessGetRequestByID($request);
            //print_r($getSerReqLog); exit;
            if($getRequestByID->request_status=="ACCEPTED" || $getRequestByID->request_status=="REACHED")
            {
                $getRequestByID->request_status="CANCELBYDRIVER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="ONRIDE"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYPASSENGER"],$status="ONRIDE");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="ONRIDE");
                    }
                }
                $cancelAmount=$SettingServices->getValueByKey("driver_cancellation_charge");
                $cancelationCharger=[
                "request_id"=>$request->request_id,
                'ride_insurance'=>0,
                'per_minute'=>0,
                'per_distance_km'=>0,
                'minimum_waiting_time_in_minutes'=>0,
                'waiting_charge_per_min'=>0,
                'waitTime'=>0,
                'duration_hr'=>0,
                'duration_min'=>0,
                'duration_sec'=>0,
                'duration'=>0,
                'distance_km'=>0,
                'distance_miles'=>0,
                'distance_meters'=>0,
                "cost"=>$cancelAmount,
                "payment_method"=>"NONE",
                "payment_gateway_charge"=>0,
                'types'=>'CREDIT',
                "transaction_type"=>"CANCELBYDRIVERCHARGE",
                "is_paid"=>"N",
                "status"=>"PENDING"
                ];
                $TransactionLogService->createCreditNoteForRide($cancelationCharger);
                $message=trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_DRIVER", [ 'cancelAmount' =>$cancelAmount, 'currency' => '$' ]);//"Ride request cancelled by you. The cancellation charge of $cancelAmount ₦ will be deducted from your wallet.";
                $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                        "text"=>$message,
                        "body"=>$message,
                        "type"=>"CANCELBYDRIVER",
                        "user_id"=>$getRequestByID->driver_id,
                        "setTo"=>'SINGLE'
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                            "type"=>"CANCELBYDRIVER",
                            "user_id"=>$getRequestByID->passenger_id,
                            "setTo"=>'SINGLE'
                            ];
                            $NotificationServices->sendPushNotification((object)$pushDate);
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
            }
            else{

                $message=trans("api.SYSTEM_MESSAGE.ON_RIDE");
            }
            return response(['message'=>$message,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    

}
