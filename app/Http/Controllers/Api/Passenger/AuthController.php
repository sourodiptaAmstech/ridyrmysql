<?php
namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\AuthService;
use Illuminate\Support\Facades\Auth;
use App\Services\PassengersProfileService;
use App\Services\EmergencyContact;
use App\Services\UsersDevices;
use App\Http\Controllers\Api\Passenger\RegistrationController;
use Validator;

class AuthController extends Controller
{
    public function login(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'social_unique_id' => ['required_if:login_by,facebook,google'],
                'username'=>'required_if:login_by,manual',
                'password' => 'required_if:login_by,manual|min:6',
                'timeZone'=>'required',
                'device_type' => 'required|in:android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $AuthService=new AuthService();
            $Profile=new PassengersProfileService();
            $UserDevice=new UsersDevices();
            $EmergencyContact=new EmergencyContact();
            $AuthData=[];
            $AuthData=$AuthService->accessLogin($request);
            if($AuthData['statusCode']==200){
                $UserAccessToken=$AuthData['data']->access_token;
                $token_type=$AuthData['data']->token_type;
                $request->user_id=$AuthData['data']->user_id;
                
                $ProfileData=$Profile->accessGetProfile($request);
                $DevicesData=$UserDevice->accessUpdateDevices($request);
                $EmergencyContactData=$EmergencyContact->accessGetContact($request);
                
                // check for the mobile is verifived of not 
                
                if((int)$ProfileData['data']->isMobileverified===0){
                    // logout the loged in user and login the user with temp accesss 
                    $UserToken=$AuthService->reLogin((object)['user_id'=>$request->user_id,"user_scope"=>"temporary-customer-service"]);
                    $UserAccessToken=$UserToken['data']->access_token;
                    $token_type=$UserToken['data']->token_type;
                    $request->user_id=$AuthData['data']->user_id;
                    
                    //logut all access user 
                   $AuthService->accesslogoutTempUser((object)['user_id'=>$request->user_id,"user_scope"=>"passenger-service"]);;
                }
                return response(['message'=>"Loged in!","data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);
            }
         /*   else if($AuthData['statusCode']==401 && ($request->login_by=="facebook" || $request->login_by=="google")){
                $RegistrationController=new RegistrationController();
                $RegistrationSocial=$RegistrationController->register($request);
                return $RegistrationSocial;
            }
            */
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }

    }
    public function logout(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $AuthService=new AuthService();
            $AuthData=$AuthService->accessLogout($request);
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
    public function forgetPassword(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'email_id' => 'required|email|max:255',      
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $AuthService=new AuthService();
            $AuthData=$AuthService->accessForgetPassword($request);
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
    public function resetPassword(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'email_id' => 'required|email|max:255',      
                'timeZone'=>'required',
                'password' => 'required|between:6,255|confirmed'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $AuthService=new AuthService();
            $AuthData=$AuthService->accessResetPassword($request);
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
}
