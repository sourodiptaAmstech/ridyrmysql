<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\LeasingService;

use Validator;



class LeasingController extends Controller
{
    public function hourlyRequest(request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'dateOfJourny' =>'required',
                'fromTime' => 'required',
                'toTime' => 'required',
                'services_type_id'=>'required',
                'address' => 'sometimes',
                'longitude'=>'required',
                'latitude'=>'required',
                'no_passenger'=>'required',
                'details'=>'required',
                'timeZone'=>'timeZone'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->passenger_id=Auth::user()->id;


            $request->from_datetime =date('Y-m-d H:i:s',strtotime($request->dateOfJourny." ".$request->fromTime));
            $request->to_datetime =date('Y-m-d H:i:s',strtotime($request->dateOfJourny." ".$request->toTime));
            $request->lease_type="hourly";
            $LeasingService=new LeasingService();
            $LeasingServiceReturn=$LeasingService->accessCreateRequest($request);







            return response(['message'=>"Your Request for the lease is successfully placed.","data"=>(object)$LeasingServiceReturn,"errors"=>array("exception"=>["OK"],"e"=>[])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function dailyRequest(request $request){
        try{
            $rule=[
                'dateOfJourny' =>'required',
                'services_type_id'=>'required',
                'address' => 'sometimes',
                'longitude'=>'required',
                'latitude'=>'required',
                'no_passenger'=>'required',
                'details'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->passenger_id=Auth::user()->id;

            //$request->from_datetime =date('Y-m-d h:m:s',strtotime($request->dateOfJourny));
            $request->lease_type="daily";
            $LeasingService=new LeasingService();
            $LeasingServiceReturn=$LeasingService->accessCreateRequest($request);

            return response(['message'=>"Your Request for the lease is successfully placed.","data"=>(object)$LeasingServiceReturn,"errors"=>array("exception"=>["OK"],"e"=>[])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function longRequest(request $request){
        try{
            $rule=[
                'fromdate' =>'required',
                'todate' =>'required',
                'services_type_id'=>'required',
                'address' => 'sometimes',
                'longitude'=>'required',
                'latitude'=>'required',
                'details'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->passenger_id=Auth::user()->id;

            $request->from_datetime =date('Y-m-d H:i:s',strtotime($request->fromdate));
            $request->to_datetime =date('Y-m-d H:i:s',strtotime($request->todate));
            $request->lease_type="longTime";
            $request->no_passenger=0;
            $LeasingService=new LeasingService();
            $LeasingServiceReturn=$LeasingService->accessCreateRequest($request);

            return response(['message'=>"Your Request for the lease is successfully placed.","data"=>(object)$LeasingServiceReturn,"errors"=>array("exception"=>["OK"],"e"=>[])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
}
