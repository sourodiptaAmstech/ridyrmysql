<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\EstimatedFareService;
use App\Services\ServiceRequestService;
use App\Services\PreferencesService;
use App\Services\SettingServices;
use App\Services\LocationService;
use App\Services\RequestPreferencesService;
use App\Services\RequestServices;
use App\Services\DriversProfileService;
use App\Services\TransactionLogService;
use App\Services\NotificationServices;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Model\Request\SplitFareRequest;

use Validator;



class RequestController extends Controller
{
    public function estimated_old(Request $request){
        try{
            $rule=[
                'source' =>'required',
                'destination' => 'required',
                'waypoint' => 'sometimes',
                'preferences' => 'sometimes',
                'request_type'=>'required|in:RideNow,Schedule'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="SERVICESEARCH";
            $request->locationDetails=["source"=>$request->source,"waypoint"=>$request->waypoint,"destination"=>$request->destination];
            $ServiceRequest=new ServiceRequestService();


            // find open request and on going trip

            // $ServiceRequest->request_no=$data->request_no;

           $ServiceRequestResult=$ServiceRequest->accessCreateRequest($request);

           $request->request_id=$ServiceRequestResult->request_id;
           $request->request_no="RIDYR"."/".date("ymd/hms");

           $ServiceRequestResult=$ServiceRequest->accessUpdateRequestNo($request);

           $EstimatedFareService =new EstimatedFareService();
           $retunEsti=$EstimatedFareService->accessGetFare($request);

           // print_r($retunEsti['message']);

            //accessGetFare
            return response(['message'=>$retunEsti['message'],"data"=>$retunEsti['data'],"errors"=>$retunEsti['errors']],$retunEsti['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function estimated(request $request){
        try{
            $rule=[
                'source' =>'required',
                'destination' => 'required',
                'waypoint' => 'sometimes',
                'preferences' => 'sometimes',
                'request_type'=>'required',
                'bags_number'=>'sometimes',
                "radio"=>'sometimes'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->user_id=Auth::user()->id;
            $request->driver_id=null;
            $EstimatedFareService =new EstimatedFareService();
            $findLastPaymentOption=$EstimatedFareService->accesslastPayment($request);
            //  exit;
            $request->request_status="SERVICESEARCH";
            $request->locationDetails=["source"=>$request->source,"waypoint"=>$request->waypoint,"destination"=>$request->destination];
           // $staticMap=$this->generateStaticMap($request->source["latitude"],$request->source["longitude"],$request->destination["latitude"],$request->destination["longitude"]);
            $request->static_map="";//$staticMap['static_map'];
            $request->route_key="";//$staticMap['route_key'];
            if(isset($request->bags_number)){
                if((int)$request->bags_number>0)
                 $request->bags_number=0;
                   else
                 $request->bags_number=0;
            }
            else {
                $request->bags_number=0;
            
            }
            

            $ServiceRequest=new ServiceRequestService();
            $ServiceRequestResult=$ServiceRequest->accessCreateRequest($request);//echo 2; exit;
            $request->request_id=$ServiceRequestResult->request_id;
            $request->request_no="RIDYR"."/".date("ymd")."/".$request->request_id;
            $ServiceRequestResult=$ServiceRequest->accessUpdateRequestNo($request);
            $SettingServices=new SettingServices();

            /** Save the requested locations */

            $LocationService = new LocationService();
            $locationArray=[];
            // sources
            $locationArray[]=array(
                "request_id"=>$request->request_id,
                "longitude"=>$request->source["longitude"],
                "latitude"=>$request->source["latitude"],
                "address"=>$request->source["address"],
                "types"=>"source",
                "orders"=>0
            );
            // waypoint
            foreach($request->waypoint as $key=>$val){
                $locationArray[]=array(
                    "request_id"=>$request->request_id,
                    "longitude"=>$val["longitude"],
                    "latitude"=>$val["latitude"],
                    "address"=>$val["address"],
                    "types"=>"waypoint",
                    "orders"=>$val["order"]
                );
            }
            // destination
            $locationArray[]=array(
                "request_id"=>$request->request_id,
                "longitude"=>$request->destination["longitude"],
                "latitude"=>$request->destination["latitude"],
                "address"=>$request->destination["address"],
                "types"=>"destination",
                "orders"=>0
            );
           // print_r($locationArray); exit;
            $LocationServiceReturn=$LocationService->accessCreateLocation($locationArray);
            $request->locationDetails=$locationArray;

            // getPreferencesByIn 
            $getPreference=[];
            if(isset($request->preferences)){
                $PreferencesService=new PreferencesService();
                $getPreference=$PreferencesService->getPreferencesByIn($request->preferences);
                $RequestPreferencesService = new RequestPreferencesService();
                foreach($getPreference as $key=>$val){
                    $val['request_id']=$request->request_id;
                   // print_r($val); exit;
                    $RequestPreferencesService->accessCreatePreferences((object)$val);
                }
            }
             // getFavDriver 
          /*   $getFavDriver=[];
             if(isset($request->preferences)){
                 $PreferencesService=new PreferencesService();
                 $getPreference=$PreferencesService->getPreferencesByIn($request->preferences);
                 $RequestPreferencesService = new RequestPreferencesService();
                 foreach($getPreference as $key=>$val){
                     $val['request_id']=$request->request_id;
                    // print_r($val); exit;
                     $RequestPreferencesService->accessCreatePreferences((object)$val);
                 }
             } */
            $retunEsti=$EstimatedFareService->accessGetFare($request);


         //  $retunEsti['data']['isFemaleFriendly']="Y";

           // print_r($retunEsti['message']);

            //accessGetFare
            return response(['message'=>$retunEsti['message'],"data"=>$retunEsti['data'],"last_paid_by"=>$findLastPaymentOption,"isFemaleFriendlyFeatureOn"=>$SettingServices->getValueByKey("female_friendly"),"errors"=>$retunEsti['errors']],$retunEsti['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function requestRide(Request $request){
        try{
            $rule=[
                'request_id' =>'required',
                'service_type_id' => 'required',
                'isFemaleFriendly'=>'required|in:Y,N',
                'payment_method'=>'required|in:CASH,CARD',
                'card_id'=>['required_if:payment_method,CARD']
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="RIDESEARCH";
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);

          //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="RIDENOW"){
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $request_status=$request->request_status;
                        $message="Thank you for choosing us, we are connecting with near by drivers.";
                    break;
                    case "RIDESEARCH":
                        $request_status=$getRequestData->request_status;
                        $message="We are connecting with near by drivers, we appreciate your patience.";
                    break;
                    }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function getPreferences(Request $request){
        try{
            $PreferencesService=new PreferencesService();
            $Preferences=$PreferencesService->accessGetPreferences();
            return response(['message'=>$Preferences['message'],"data"=>(object)$Preferences['data'],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function cancelRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];


            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get request details,
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $DriverProfile=new DriversProfileService();
            $SettingServices=new SettingServices();
            $TransactionLogService=new TransactionLogService();
            $NotificationServices =new NotificationServices();

            $getRequestByID=$ServiceRequestService->accessGetRequestByID($request);
            //print_r($getSerReqLog); exit;
            if($getRequestByID->request_status=="SERVICESEARCH" || $getRequestByID->request_status=="RIDESEARCH" ){
                $getRequestByID->request_status="CANCELBYPASSENGER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="REQUESTED"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYPASSENGER"],$status="REQUESTED");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="REQUESTED");
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "type"=>"CANCELBYPASSENGER",
                            "user_id"=>$val['driver_id'],
                            "setTo"=>'SINGLE'
                            ];
                            $NotificationServices->sendPushNotification((object)$pushDate);
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    }
                }
                $message=trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_PASSENGER");
                $pushDate=[
                    "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                    "text"=>$message,
                    "body"=>$message,
                    "type"=>"CANCELBYPASSENGER",
                    "user_id"=>$getRequestByID->passenger_id,
                    "setTo"=>'SINGLE'
                    ];
                    $NotificationServices->sendPushNotification((object)$pushDate);
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);

            }
            else if($getRequestByID->request_status=="ACCEPTED" || $getRequestByID->request_status=="REACHED")
            {
                $getRequestByID->request_status="CANCELBYPASSENGER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="ONRIDE"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYPASSENGER"],$status="ONRIDE");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="ONRIDE");
                    }
                }
                $cancelAmount=$SettingServices->getValueByKey("passenger_cancellation_charge");
                $cancelationCharger=[
                "request_id"=>$request->request_id,
                'ride_insurance'=>0,
                'per_minute'=>0,
                'per_distance_km'=>0,
                'minimum_waiting_time_in_minutes'=>0,
                'waiting_charge_per_min'=>0,
                'waitTime'=>0,
                'duration_hr'=>0,
                'duration_min'=>0,
                'duration_sec'=>0,
                'duration'=>0,
                'distance_km'=>0,
                'distance_miles'=>0,
                'distance_meters'=>0,
                "cost"=>$cancelAmount,
                "payment_method"=>"NONE",
                "payment_gateway_charge"=>0,
                'types'=>'CREDIT',
                "transaction_type"=>"CANCELBYPASSENGERCHARGE",
                "is_paid"=>"N",
                "status"=>"PENDING"
                ];
                $TransactionLogService->createCreditNoteForRide($cancelationCharger);
                $message= trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_PASSENGER_WITH_FEE", [ 'cancelAmount' =>$cancelAmount, 'currency' => '$' ]);  //"Your request has been cancelled and $cancelAmount ₦ will be charged from your next ride as the cancellation fee";
                $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                        "text"=>$message,
                        "body"=>$message,
                        "type"=>"CANCELBYPASSENGER",
                        "user_id"=>$getRequestByID->passenger_id,
                        "setTo"=>'SINGLE'
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "type"=>"CANCELBYPASSENGER",
                            "user_id"=>$getRequestByID->driver_id,
                            "setTo"=>'SINGLE'
                            ];
                            $NotificationServices->sendPushNotification((object)$pushDate);
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
            }
            else{
                $message="It's seems your onride already, you cancel ride cannot be processed.";
            }
            return response(['message'=>$message,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function ratingComment(Request $request){
        try{
            $rule=[
                'request_id' =>'required',
                'rating'=>'required',
                'comment'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);

            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->passenger_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();

            $ServiceRequestServiceReturn = $ServiceRequestService->accessAddPassengerRatingComment((object)[
                "request_id"=>$request->request_id,
                "rating_by_passenger"=>$request->rating,
                "comment_by_passenger"=>$request->comment,
                "passenger_rating_status"=>"GIVEN",
                "passenger_id"=>$request->user_id
                ]);
            // calculate over all ratting.



            $message="Thank you, for rating the driver.";
            $status="RATING";
            $nextStep="COMPLETED";

            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["ok"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function getCmsonTrip(Request $request){
        $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'key'=>'required'
            ];
            $SettingServices =new SettingServices();
            return response(['message'=>"","data"=>(object)["content"=>$SettingServices->getValueByKey($request->key)],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);

    }
    public function splitSearch(Request $request){
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $rule=[
            'timeZone'=>'required',
            'searchQuery'=>'required'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
        $request->user_id=Auth::user()->id;       
        $db=DB::select('SELECT * FROM passengers_profile WHERE passengers_profile.first_name LIKE "%'.$request->searchQuery.'%" or passengers_profile.last_name   LIKE "%'.$request->searchQuery.'%" or passengers_profile.mobile_no LIKE "%'.$request->searchQuery.'%" and passengers_profile.user_id<>'.$request->user_id.'  ORDER BY passengers_profile.first_name ASC' );
        
        return response(['message'=>"","data"=>$db,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
    }
    public function requestSplit(Request $request){
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $rule=[
            'timeZone'=>'required',
            'requested_user_id'=>'required',
            'request_id'=>'required'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
        $request->user_id=Auth::user()->id;       
       
        $pushDate=[
            "title"=>"Split Fare",
            "text"=>"Your friend send you a split fare request for the ongoing ride.",
            "body"=>"Your friend send you a split fare request for the ongoing ride.",
            "type"=>"SPLITFARE",
            "user_id"=>$request->user_id,
            "setTo"=>'SINGLE'
            ];
            $SplitFareRequest=new SplitFareRequest();
            $SplitFareRequest->requested_by_id=$request->user_id;
            $SplitFareRequest->requested_to_id =$request->requested_user_id;
            $SplitFareRequest->request_id =$request->request_id;
            $SplitFareRequest->accpected="Send";// status is send 
            $SplitFareRequest->save();

            $NotificationServices =new NotificationServices();
            $NotificationServices->sendPushNotification((object)$pushDate);
            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
        return response(['message'=>"Split Fare Request is send to your friend","data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
    }

    public function requestSplitAccptReject(Request $request){
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $rule=[
            'timeZone'=>'required',
            'sender_user_id'=>'required',
            'request_id'=>'required',
            'request_status'=>'required|in:Yes,No,NotReply'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
        $request->user_id=Auth::user()->id;   
        $SplitFareRequest= SplitFareRequest::where("requested_by_id",$request->sender_user_id)->where('request_id',$request->request_id)->where('requested_to_id',$request->user_id)->first();    
        $SplitFareRequest->accpected=$request->request_status;
        $SplitFareRequest->save();
        $mess="Your friend had accpected your a split fare request.";
        if($request->request_status=="Yes"){
            $mess="Your friend had accpected your a split fare request.";
        }
        if($request->request_status=="No"){
            $mess="Your friend had rejected your a split fare request.";
        }
        if($request->request_status=="NotReply"){
            $mess="Your friend had rejected your a split fare request.";
        }
        $pushDate=[
            "title"=>"Split Fare",
            "text"=>$mess,
            "body"=>$mess,
            "type"=>"SPLITFARE",
            "user_id"=>$request->sender_user_id,
            "setTo"=>'SINGLE'
            ];

            $NotificationServices =new NotificationServices();
            $NotificationServices->sendPushNotification((object)$pushDate);
            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
        return response(['message'=>"Thanks you for responding to split fare request","data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
    }



}
