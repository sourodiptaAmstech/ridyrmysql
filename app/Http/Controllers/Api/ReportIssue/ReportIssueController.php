<?php
namespace App\Http\Controllers\Api\ReportIssue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\ReportIssue\ReportIssue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Storage;


class ReportIssueController extends Controller{
    public function reportIssue(Request $request){
        $this->validate($request, [
            'subject' => 'required',
            'description'=>'required',
            'reportBy'=>'required',
        ]);
        try{

            $ReportIssue=new ReportIssue();
            $ReportIssue->subject=$request->subject;
            $ReportIssue->description=$request->description;
            $ReportIssue->reportedby=$request->reportBy;
            $ReportIssue->user_id=Auth::user()->id;
            $ReportIssue->save();


            if($ReportIssue->issues_id>0){
                return response(['message'=>"Your issue has been reported. We will contact you soon","data"=>(object)[],"errors"=>array("exception"=>["Return Expection"],"e"=>[])],201);
            }
            else{
                return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>["Return Expection"],"e"=>[])],400);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"],"e"=>$e)],401);
        }
        catch(\Exception $e){
            // return response()->json(['error' => $e->getMessage()]);
            return response(['message'=>'Something went wrong',"errors"=>array("exception"=>["Exception"],"e"=>$e)],500);
        }
    }
}
