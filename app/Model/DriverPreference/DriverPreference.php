<?php

namespace App\Model\DriverPreference;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Notifications\Notifiable;
class DriverPreference extends Model
{
    //
    //use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'drivers_preference';
    protected $primaryKey = 'drivers_preference_id';
}
