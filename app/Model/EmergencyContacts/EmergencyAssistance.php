<?php

namespace App\Model\EmergencyContacts;

use Illuminate\Database\Eloquent\Model;

class EmergencyAssistance extends Model
{
   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'emergency_assistances';
    protected $primaryKey = 'emergency_assistance_id';
    
}
