<?php

namespace App\Model\Leasing;

use Illuminate\Database\Eloquent\Model;

class Leasing extends Model
{
    protected $table = 'leasing';
    protected $primaryKey = 'leasing_id';
}
