<?php

namespace App\Model\Preference;

use Illuminate\Database\Eloquent\Model;

class PassengerFavDriver extends Model
{
  
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'passenger_fav_driver';
    protected $primaryKey = 'pass_fav_id';
}
