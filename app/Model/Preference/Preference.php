<?php

namespace App\Model\Preference;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    //
    //use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'preference_mst';
    protected $primaryKey = 'preference_id';

    public function driver_preference(){
        return $this->hasMany('App\Model\DriverPreference\DriverPreference','preference_id','preference_id');
    }
    
 }
