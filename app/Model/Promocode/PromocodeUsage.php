<?php

namespace App\Model\Promocode;

use Illuminate\Database\Eloquent\Model;

class PromocodeUsage extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promocode_usages';
    protected $primaryKey = 'promocode_usages_id';
}
