<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class RequestFavDriver extends Model
{
  
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'request_fav_driver';
    protected $primaryKey = 'req_fav_id';
}
