<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class RequestPreferences extends Model
{
  
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'request_preferences';
    protected $primaryKey = 'req_pref_id';
}
