<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;
class SplitFareRequest extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'split_fare_request';
    protected $primaryKey = 'split_fare_request_id';


}
