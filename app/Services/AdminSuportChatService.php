<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Message\adminChatSupport;



class AdminSuportChatService
{
    private function createMessageAdminSupport($data){
       $adminChatSupport=new adminChatSupport();
       $adminChatSupport->user_id=$data->user_id;
       $adminChatSupport->user_scope=$data->user_scope;
       $adminChatSupport->message=$data->message;
       $adminChatSupport->thread_id=$data->thread_id;
       $adminChatSupport->save();
       return $adminChatSupport;
    }

    private function getMessageForSupport($thread_id,$timeZone){
       // echo $timeZone."===================";
        $timeZone=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        //print_r($timeZone);
        $messageList = DB::select('SELECT admin_support.message_id as message_id,admin_support.thread_id as thread_id
        ,admin_support.user_id as user_id ,
        admin_support.user_scope as user_scope,
        admin_support.message as `message`,
        CONVERT_TZ(admin_support.updated_at,"+00:00","'.$timeZone[0].':'.$timeZone[1].'") as updated_at,
        CONVERT_TZ(admin_support.created_at,"+00:00","'.$timeZone[0].':'.$timeZone[1].'") as created_at,
        (CASE
            WHEN admin_support.user_scope="passenger-service"
            THEN
            passengers_profile.first_name
            WHEN admin_support.user_scope="driver-service"
            THEN
            drivers_profile.first_name
            ELSE
            admins_profile.first_name
        END) as fname,
        (CASE
            WHEN admin_support.user_scope="passenger-service"
            THEN
            passengers_profile.last_name
            WHEN admin_support.user_scope="driver-service"
            THEN
            drivers_profile.last_name
            ELSE
            admins_profile.last_name
        END) as lname,
        (CASE
            WHEN admin_support.user_scope="passenger-service"
            THEN
            passengers_profile.picture
            WHEN admin_support.user_scope="driver-service"
            THEN
            drivers_profile.picture
         ELSE
            admins_profile.picture
        END) as picture
        from admin_support
        LEFT JOIN passengers_profile on passengers_profile.user_id=admin_support.user_id
        LEFT JOIN drivers_profile on drivers_profile.user_id=admin_support.user_id
        LEFT JOIN admins_profile on admins_profile.user_id=admin_support.user_id
        where admin_support.thread_id=?
        ORDER BY admin_support.message_id ASC',[$thread_id]);

        return $messageList;
    }

    public function accessCreateMessageAdminSupport($data){
        return $this->createMessageAdminSupport($data);
    }
    public function accessGetMessageForSupport($thread_id,$timeZone){
        return $this->getMessageForSupport($thread_id,$timeZone);
    }
}
