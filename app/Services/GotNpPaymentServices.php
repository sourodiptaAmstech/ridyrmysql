<?php

namespace App\Services;

use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Profiles\PassengersProfile;

class GotNpPaymentServices
{
    public function __construct(){
        // $Setting=Setting::where("key","payment_gateway_mode")->select("value")->first();
    }
    private function curlMethods($data){
        try{
            $curl = curl_init();
          //  dd($data->CURLOPT_POSTFIELDS);
            //$url=; //exit;

            if($data->methord=="POST"){
                curl_setopt_array($curl,array(
                    CURLOPT_URL => "https://sandbox.gotnpgateway.com/api$data->url",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $data->methord,
                    CURLOPT_POSTFIELDS=>$data->CURLOPT_POSTFIELDS,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: api_1ePCHRXBO1wBwIAwF74KWBdg2pr",
                        "Content-Type: application/json"
                    )));
                }
                else  if($data->methord=="DELETE"){
                    curl_setopt_array($curl,array(
                        CURLOPT_URL => "https://sandbox.gotnpgateway.com/api$data->url",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $data->methord,
                     //   CURLOPT_POSTFIELDS=>$data->CURLOPT_POSTFIELDS,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: api_1ePCHRXBO1wBwIAwF74KWBdg2pr",
                            "Content-Type: application/json"
                        )));
                    }
                else{
                    curl_setopt_array($curl,array(
                        CURLOPT_URL => "https://sandbox.gotnpgateway.com/api".$data->url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $data->methord,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: api_1ePCHRXBO1wBwIAwF74KWBdg2pr",
                            "Content-Type: application/json"
                        )));
                }

                $response = curl_exec($curl);
               // print_r($response); exit;
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                $response=json_decode($response,true);
                return ['message'=>"","data"=>$response,"errors"=>"",'statusCode'=>$httpcode];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
    }

    public function verifyTranscationByRefrence($data){
        return $this->curlMethods((object)[
            'url'=>'/transaction/'.$data->reference,
            'methord'=>'GET'
            ]);
    }

    public function deleteCards($data){
       
        $PassengersProfile=PassengersProfile::where("user_id",$data->user_id)->first();
        return $this->curlMethods((object)[
            'url'=>"customer/$PassengersProfile->payment_gateway_customer_id/paymentmethod/card/$data->authorization_code",
            'methord'=>'DELETE',
            ]);
    }

    public function makePayment($data){
       // print_r($data); exit;
       $CURLOPT_POSTFIELDS=array(
           "amount"=> $data->amount,
           "type"=> "sale",
           "currency"=> "USD",
           "description"=> "test transaction",
           "order_id"=> "ord1",
           "po_number"=>"somePONumber",
           "email_receipt"=> false,
           "create_vault_record"=> false,
           "payment_method"=> [
               "customer"=> [
                   "id"=>"budaqs1erttrncgjvv30"
                   ]
                ]
            );
            return $this->curlMethods((object)[
            'url'=>'/transaction',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

    public function addCard($data){
        // check customer id is created or not passenger
        $PassengersProfile=PassengersProfile::where("user_id",$data->user_id)->first();
        $CURLOPT_POSTFIELDS['CURLOPT_POSTFIELDS']=[];
        $CURLOPT_POSTFIELDS['url']="";
        $CURLOPT_POSTFIELDS['methord']="";
        $return=['message'=>"","data"=>[],"errors"=>"",'statusCode'=>400,"customer"=>false];
        if($PassengersProfile->payment_gateway_customer_id=="" || $PassengersProfile->payment_gateway_customer_id==null){
            $CURLOPT_POSTFIELDS["CURLOPT_POSTFIELDS"]=json_encode([
                "payment_method"=>[
                    "card"=> [ 
                        "card_number"=>$data->card_number , 
                        "expiration_date"=>$data->expiration_date 
                    ]
                    ],
                    "billing_address"=> [
                        "first_name"=> $PassengersProfile->first_name, 
                        "last_name"=>$PassengersProfile->last_name, 
                        "email"=> $PassengersProfile->email_id, 
                        ]
            ]);
            $CURLOPT_POSTFIELDS['url']="/customer";
            $CURLOPT_POSTFIELDS['methord']="POST";
            $return["customer"]=true;
        }
        else{

            $CURLOPT_POSTFIELDS["CURLOPT_POSTFIELDS"]=json_encode([
                "card_number"=>$data->card_number , 
                "expiration_date"=>$data->expiration_date 
            ]);
            $CURLOPT_POSTFIELDS['url']="/customer/$PassengersProfile->payment_gateway_customer_id/paymentmethod/card "; 
            $CURLOPT_POSTFIELDS['methord']="POST";
            
        }
        $curlResult=$this->curlMethods((object)$CURLOPT_POSTFIELDS);
     
        $return["message"]=$curlResult['data']['msg'];
        $return["statusCode"]=$curlResult['statusCode'];

        if($curlResult['statusCode']==200){
            $return["data"]=$curlResult['data']['data'];
            if($return['customer']== true){
                $PassengersProfile->payment_gateway_customer_id=$return["data"]['id'];
                $PassengersProfile->save();
            }
        }

        return $return;
    }



}



