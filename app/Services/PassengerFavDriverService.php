<?php

namespace App\Services;

use App\Model\Preference\PassengerFavDriver;
use Storage;

class PassengerFavDriverService
{
    
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    public function createFavDriver($data){
        $FavDriver =PassengerFavDriver::where("passenger_id",$data->user_id)->where("driver_id",$data->driver_id)->get()->toArray();
        if(count($FavDriver)>0){
            return $FavDriver;
        }
        else{
            $PassengerFavDriver = new PassengerFavDriver();
            $PassengerFavDriver->passenger_id=$data->user_id;
            $PassengerFavDriver->driver_id=$data->driver_id;
            $PassengerFavDriver->save();
            return $PassengerFavDriver;
        }
    }
    private function getFavDriver($data){
        try{
            
            $PassengerDriver = PassengerFavDriver::join('drivers_profile', 'drivers_profile.user_id','=','passenger_fav_driver.driver_id')
            ->where('passenger_fav_driver.passenger_id','=',$data->user_id)->select('passenger_fav_driver.*','drivers_profile.*')->get()->toArray();
            return ['message'=>"Passenger Favorite Driver","data"=>$PassengerDriver,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
   
    public function accessGetFavDriver($data){

        return $this->getFavDriver($data);

    }
    public function accessCreateFavDriver($data){

        return $this->createFavDriver($data);

    }
}
