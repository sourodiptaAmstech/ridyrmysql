<?php

namespace App\Services;


use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\GotNpPaymentServices;

class PaymentService
{
    private $stripe;
    public function __construct(){
        $Setting=Setting::where("key","stripe_mode")->select("value")->first();
    }
    private function listCard($data){
        try{
            $cards=UserCard::select("user_cards_id","last_four","brand","is_default","status","email","expiration_date")->where('user_id',$data->user_id)->where('status','<>',"Delete")->orderBy('created_at','desc')->get()->toArray();
            if(!empty($cards))
            return ['message'=>"Card List","data"=>$cards,"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>"No card found. Please add card.","data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>204];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found. Please add card.","data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }
    private function addCard($data){
        try{
            $exist=UserCard::where('user_id',$data->user_id)->where('status','<>',"Delete")->count();
            $create_card = new UserCard;
            $create_card->user_id = $data->user_id;
         //   $create_card->email = $data->email;
          //  $create_card->customer_code = $data->customer_code;
          //  $create_card->authorization_code = $data->authorization_code;
            $create_card->last_four = $data->last_four;
            $create_card->signature = $data->signature;
            $create_card->brand = $data->brand;
            $create_card->expiration_date=$data->expiration_date;
         //   $create_card->channel = $data->channel;
           // $create_card->country_code = $data->country_code;
           // $create_card->status = $data->status;
           if($exist == 0){
               $create_card->is_default = 1;
            }else{
                $create_card->is_default = 0;
            }
            $create_card->save();
            return ['message'=>"Card Added","data"=>$create_card,"errors"=>array("exception"=>["Resource Created"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["stripe resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }
    private function setDefaultCard($data){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->update(array('is_default' => 0));
            $UserCardSetDefault=UserCard::where('user_cards_id', $data->user_cards_id)->update(array('is_default' => 1));

            return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }
    private function getDefaultCard($data){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->where('is_default',0)->get()->toArray();


            return $UserCardUnSetDefault;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }
    private function deleteCard($data){
        try{
            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->where("user_cards_id",$data->user_cards_id)->first();
            $GotNpPaymentServices = new GotNpPaymentServices();

            $verifyArray=$GotNpPaymentServices->deleteCards($UserCardUnSetDefault);
           // dd($verifyArray);
            UserCard::where('user_id', $data->user_id)->where("user_cards_id",$data->user_cards_id)->update(array('status' => "Delete"));
            return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }
    private function makeCardPayment($data,$requestedRide){
        try{
           
            $UserCardUnSetDefault=UserCard::where('user_id', $requestedRide->user_id)->where("user_cards_id",$requestedRide->user_cards_id)->first();
           // print_r( $UserCardUnSetDefault); exit;
            $GotNpPaymentServices = new GotNpPaymentServices();
            $paymentObject=["authorization_code"=>$UserCardUnSetDefault->signature,"amount"=>((float)$data->cost-(float)$data->promo_code_value)*100,"request_no"=>$requestedRide->request_no,"email"=>$UserCardUnSetDefault->email];
            $makePayment=$GotNpPaymentServices->makePayment((object)$paymentObject);
            $reference=0;$statuCode=200;
            $data=[];
           // print_r($makePayment); echo "Iam here ";
            if($makePayment['statusCode']==200){
                if($makePayment['data']['data']['status']=="pending_settlement"){
                    $reference=$makePayment['data']['data']['id']; 
                    $verifyPayment= $GotNpPaymentServices->verifyTranscationByRefrence((object)['reference'=>$reference]);
                //  print_r($verifyPayment['data']['data']['status']); echo "Iam here ";
                  //exit;


                  //  $gateWayFee=$verifyPayment['data']['data']['fees'];
                   // $refernceTrasaction=$makePayment['data']['data']['reference'];
                    $data=[
                        'reference'=> $reference,//$makePayment['data']['data']['reference'],
                        'fees'=>0,//($gateWayFee/100),
                        'status'=>$verifyPayment['data']['data']['status']
                    ];
                }
                else{
                    // verify transaction
                  //  $verifyPayment= $GotNpPaymentServices->verifyTranscationByRefrence((object)['reference'=>$makePayment['data']['data']['reference']]);
                    $statuCode=400;
                }
            }
            else{
                $statuCode=400;
            }
           // print_r($makePayment['data']['data']['status']); exit;
            // print_r($verifyPayment); exit;
            return ['message'=>"Payment","data"=>$data,"errors"=>array("exception"=>["Payment"]),"statusCode"=>$statuCode];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }
    private function getCardDetailsByCardId($data){
        try{
            $cards=UserCard::select("user_cards_id","last_four","brand","is_default","status","email")->where('user_cards_id',$data->user_cards_id)->first();
            if(!empty($cards))
            return ['message'=>"Card List","data"=>$cards,"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>"No card found. Please add card.","data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>204];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found. Please add card.","data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }

    public function accessListCard($data){
        return $this->listCard($data);
    }
    public function accessSetDefaultCard($data){
     return $this->setDefaultCard($data);
    }
    public function accessAddCards($data){
        return $this->addCard($data);
    }
    public function accessDeleteCard($data){
        return $this->deleteCard($data);
    }
    public function accessGetDefaultCard($data){
        return $this->getDefaultCard($data);
    }

    public function accesssMakeCardPayment($data,$requestedRide){
        try{
            return $this->makeCardPayment($data,$requestedRide);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }

    }

    public function accessGetCardDetailsByCardId($data){
        return $this->getCardDetailsByCardId($data);
    }
}



