<?php

namespace App\Services;

use App\Model\Preference\Preference;
use App\Model\Request\RequestPreferences;
use Storage;

class RequestPreferencesService
{
    private $Preferences;
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    private function createPreferences($data){
        $RequestPreferences = new RequestPreferences();
        $RequestPreferences->request_id=$data->request_id;
        $RequestPreferences->preference_id=$data->preference_id;
        $RequestPreferences->preference_name =$data->preference_name ;
        $RequestPreferences->save();
        return $RequestPreferences;
    }
    private function getPreferences($data){
        try{
            $Preferences=RequestPreferences::where("request_id",$data->request_id );

            return ['message'=>"Preference Data","data"=>$Preferences,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
   
    public function accessGetPreferences($data){

        return $this->getPreferences($data);

    }
    public function accessCreatePreferences($data){

        return $this->createPreferences($data);

    }
}
