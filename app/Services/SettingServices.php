<?php

namespace App\Services;

use App\Model\Setting\Setting;

class SettingServices
{
    public function getValueByKey($key){
        $value=Setting::where("key",$key)->first();
        return $value->value;
    }
}



