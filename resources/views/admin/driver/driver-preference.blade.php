@extends('admin.layout.base')

@section('title', 'Driver Preferences ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <input type="hidden" name="driver_id" id="driver_id" value="{{$id}}">
            <div class="box box-block bg-white">
                <h5 class="mb-1">Driver Preferences</h5>
                <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>Preference Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="driver_pref">
                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Preference Name</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
getDriverPreference()
function getDriverPreference(){
    var dataObject={"data":[]};
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var driver_id = $('#driver_id').val();
    $.ajax({
        url: '{{route("admin.driver.preference.get")}}',
        method: 'post',
        data: { driver_id: driver_id },
        success: function(response){
            var objectLength=Object.keys(response.data).length;
            for(var i=0; i<objectLength; i++){
                console.log(response.data[i].checked)
                if(response.data[i].checked!='undefined'){
                    if(response.data[i].checked==1){
                        dataObject.data.push(
                        [
                            response.data[i].preference_name,
                            '<input type="checkbox" name="preference_id" id="preference_id" value="'+response.data[i].preference_id+'" checked>'
                        ]);
                    } else {
                        dataObject.data.push(
                        [
                            response.data[i].preference_name,
                            '<input type="checkbox" name="preference_id" value="'+response.data[i].preference_id+'" id="preference_id">'
                        ]);
                    }

                } else {
                    dataObject.data.push(
                    [
                        response.data[i].preference_name,
                        '<input type="checkbox" name="preference_id" value="'+response.data[i].preference_id+'" id="preference_id">'
                    ]);
                }
            }
            
            $('#table-2').DataTable().clear().destroy();
            $('#table-2').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                'data': dataObject.data,
            });
        },
        error: function(response){
        }
    });
}

    $(document).on('change','#preference_id', function(){
        if ($(this).is(":checked"))
        {
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var driver_id = $('#driver_id').val();
            var preference_id = $(this).val();
            var isChecked = 'YES';
            $.ajax({
                url: '{{route("admin.update.driver.preference")}}',
                method: 'post',
                data: { driver_id: driver_id, preference_id: preference_id, isChecked: isChecked },
                success: function(response){
                    getDriverPreference();
                },
                error: function(response){
                    console.log(response);
                }
            })
        } else {
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var driver_id = $('#driver_id').val();
            var preference_id = $(this).val();
            var isChecked = 'NO';
            $.ajax({
                url: '{{route("admin.update.driver.preference")}}',
                method: 'post',
                data: { driver_id: driver_id, preference_id: preference_id, isChecked: isChecked },
                success: function(response){
                    getDriverPreference();
                },
                error: function(response){
                    console.log(response);
                }
            })
        }
    })

 });
</script>
@endsection