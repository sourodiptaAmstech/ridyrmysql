@extends('admin.layout.base')

@section('title', 'Add Emergency Assistance ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.emergency-assistance.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Emergency Assistance</h5>

            <form class="form-horizontal" action="{{route('admin.emergency-assistance.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="emergency_assistance_name" class="col-xs-2 col-form-label">Emergency Assistance Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('emergency_assistance_name') }}" name="emergency_assistance_name" required id="emergency_assistance_name" placeholder="Emergency Assistance Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact_number" class="col-xs-2 col-form-label">Contact Number</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('contact_number') }}" name="contact_number" required id="contact_number" placeholder="Contact Number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add Emergency Assistance</button>
                        <a href="{{route('admin.emergency-assistance.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection