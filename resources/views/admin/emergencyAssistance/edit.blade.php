@extends('admin.layout.base')

@section('title', 'Update Emergency Assistance ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.emergency-assistance.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Emergency Assistance</h5>

            <form class="form-horizontal" action="{{route('admin.emergency-assistance.update', $preference->emergency_assistance_id)}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group row">
                    <label for="emergency_assistance_name" class="col-xs-2 col-form-label">Emergency Assistance Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $preference->name }}" name="emergency_assistance_name" required id="emergency_assistance_name" placeholder="Emergency Assistance Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact_number" class="col-xs-2 col-form-label">Contact Number</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $preference->contact_no }}" name="contact_number" required id="contact_number" placeholder="Contact Number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Update Emergency Assistance</button>
                        <a href="{{route('admin.emergency-assistance.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection