@extends('admin.layout.base')

@section('title', 'Emergency Assistances ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Emergency Assistances</h5>
                <a href="{{ route('admin.emergency-assistance.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Emergency Assistance</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                           <th>S.No</th>
                            <th>Emergency Assistance Name</th>
                            <th>Contact Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($emergencyAssistances as $index => $emergencyAssistance)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$emergencyAssistance->name}}</td>
                             <td>{{$emergencyAssistance->contact_no}}</td>
                            <td style="line-height: 34px;">
                                <a href="{{ route('admin.emergency-assistance.edit', $emergencyAssistance->emergency_assistance_id) }}" class="btn btn-info"> Edit</a>
                            <form class="doc-delete" action="{{ route('admin.emergency-assistance.destroy', $emergencyAssistance->emergency_assistance_id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Emergency Assistance Name</th>
                            <th>Contact Number</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.doc-delete',function (e) {
            e.preventDefault();
            var form=$(this);
            bootbox.confirm('Do you really want to delete?', function (res) {
            if (res){
               form.submit();
            }
            });
        });
    });
</script>

@endsection