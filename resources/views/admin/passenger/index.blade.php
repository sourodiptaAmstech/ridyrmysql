@extends('admin.layout.base')

@section('title', 'Passengers ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Passengers
            </h5>
            <a href="{{ route('admin.passenger.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New passenger</a>
            <table class="table table-striped table-bordered dataTable" id="table-pass">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Passenger Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Passenger Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#table-pass').DataTable( {
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-passenger')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"id"},
                {"data":"passenger_name"},
                {"data":"email"},
                {"data":"mobile"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );
    </script>
@endsection