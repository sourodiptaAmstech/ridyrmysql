@extends('admin.layout.base')

@section('title', 'Add Preference ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.preference.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Preference</h5>

            <form class="form-horizontal" action="{{route('admin.preference.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="preference_name" class="col-xs-2 col-form-label">Preference Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('preference_name') }}" name="preference_name" required id="preference_name" placeholder="Preference Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add Preference</button>
                        <a href="{{route('admin.preference.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection