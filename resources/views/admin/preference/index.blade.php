@extends('admin.layout.base')

@section('title', 'Preferences ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Preferences</h5>
                <a href="{{ route('admin.preference.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Preference</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Preference Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($preferences as $index => $preference)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$preference->preference_name}}</td>
                            <td style="line-height: 34px;">
                                <a href="{{ route('admin.preference.edit', $preference->preference_id) }}" class="btn btn-info"> Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Preference Name</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection