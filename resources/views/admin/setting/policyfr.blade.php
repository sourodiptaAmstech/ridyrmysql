@extends('admin.layout.base')

@section('title', 'Privacy Policy ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Privacy Policy (French)</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'page_privacy_fr')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update.french',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="mmyeditor">{{$policy->value}}</textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Passenger's Terms & Conditions (French)</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'condition_privacy_fr')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update.french',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="mmyeditor1">{{$policy->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Driver's Terms & Conditions (French)</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'condition_privacy_driver_fr')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update.french',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="mmyeditor2">{{$policy->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('mmyeditor');
    CKEDITOR.replace('mmyeditor1');
    CKEDITOR.replace('mmyeditor2');
</script>
@endsection