@extends('admin.layout.base')

@section('title', 'Safety Tools')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Learn About Safety</h5>

            @foreach($safetys as $safety)
            @if($safety->key == 'learn_about_safety')
            <div className="row">
                <form action="{{ route('admin.safety-tools.update',$safety->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor">{{$safety->value}}</textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Safety Tools</h5>

            @foreach($safetys as $safety)
            @if($safety->key == 'system_tool')
            <div className="row">
                <form action="{{ route('admin.safety-tools.update',$safety->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor1">{{$safety->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
    CKEDITOR.replace('myeditor1');
</script>
@endsection