<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

//Route::get('bootstrap', 'Api\SettingController@bootstrap')->name("setting.bootstrap");
Route::group(['middleware' => ['ChangeLanguage']], function () {

Route::post('passenger/signup', 'Api\Passenger\RegistrationController@register')->name("passenger.registration");
Route::post('passenger/login', 'Api\Passenger\AuthController@login')->name("passenger.login");
Route::post('passenger/forget/password', 'Api\Passenger\AuthController@forgetPassword')->name("passenger.forgetPassword");
Route::post('passenger/reset/password', 'Api\Passenger\AuthController@resetPassword')->name("passenger.resetPassword");
Route::post('payment/verify/transaction', 'Api\Payment\PayStackController@verifyTransaction')->name("verifyTransaction");

Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
    //Mobile no update and verification
    Route::post('passenger/mobile', 'Api\Passenger\ProfileController@updateMobileNo')->name("passenger.mobile");
    Route::put('passenger/mobile/verify', 'Api\Passenger\ProfileController@verifyMobileNo')->name("passenger.mobileVerify");
    Route::get('passenger/mobile/reverify', 'Api\Passenger\ProfileController@resetMobileVerificationOtp')->name("passenger.mobileReVerify");
    Route::post('passenger/profile/image/update', 'Api\Passenger\ProfileController@profileImageUpdate')->name("passenger.profileImage");
});
Route::group(['middleware' => ['auth:api','scope:passenger-service']], function () {
    //logout
    Route::get('passenger/logout', 'Api\Passenger\AuthController@logout')->name("passenger.logout");

    //profile images update
    Route::post('passenger/profile/image', 'Api\Passenger\ProfileController@profileImageUpdate')->name("passenger.profileImage");

    //profile update  getProfile
    Route::get('passenger/profile', 'Api\Passenger\ProfileController@getProfile')->name("passenger.profile");
    Route::post('passenger/profile/update', 'Api\Passenger\ProfileController@updateProfile')->name("passenger.updateProfile");


    // password
    Route::post('passenger/change/password', 'Api\Passenger\ProfileController@changePassword')->name("passenger.changePassword");

    //ride request.
    Route::post('passenger/get/estimated/fare','Api\Passenger\RequestController@estimated')->name("passenger.estimatedFare");
    Route::post('passenger/request/ride','Api\Passenger\RequestController@requestRide')->name("passenger.requestRide");
    Route::get('passenger/preferences','Api\Passenger\RequestController@getPreferences')->name("passenger.getPreferences");

    // background api
    Route::post('passenger/background/data', 'Api\Background\PassengerBackgroundController@get')->name("passenger.backgroundDb");

     // trasctions
     //Route::post('passenger/transaction/email','Api\Passenger\ProfileController@updateTranscationEmail')->name("passenger.updateTranscationEmail");
     Route::get('passenger/card', 'Api\Payment\PaymentController@ListCard')->name("passenger.ListCard");
     Route::post('passenger/card', 'Api\Payment\PaymentController@addCard')->name("passenger.addCard");
     Route::put('passenger/default/card', 'Api\Payment\PaymentController@setDefaultCard')->name("passenger.setDefaultCard");
     Route::delete('passenger/card', 'Api\Payment\PaymentController@DeleteCard')->name("passenger.delete");
     Route::get('passenger/get/transaction/list', 'Api\Payment\PaymentController@getCustomerTransaction')->name("passenger.getCustomerTransaction");
 
     // promocode
     Route::post('passenger/add/promo', 'Api\Promocode\PromocodeController@addPromoCode')->name("passenger.addPromoCode");
     Route::get('passenger/get/promo', 'Api\Promocode\PromocodeController@getPromoCode')->name("passenger.getPromoCode");
     // ride cancelation
     Route::post('passenger/ride/cancel', 'Api\Passenger\RequestController@cancelRide')->name("passenger.cancelRide");

     Route::get('passenger/get/favorite/driver', 'Api\Passenger\ProfileController@getFavDriver')->name("passenger.getFavDriver");
     Route::post('passenger/add/favorite/driver', 'Api\Passenger\ProfileController@addFavDriver')->name("passenger.addFavDriver");
     Route::get('passenger/emergency/assistance', 'Api\Passenger\ProfileController@emergencyAssistances')->name("passenger.emergencyAssistances");
     // update langage seletion
     Route::put('passenger/update/lang', 'Api\Passenger\ProfileController@updateLang')->name("passenger.lang");

     Route::post('passenger/request/ratingComment','Api\Passenger\RequestController@ratingComment')->name("passenger.ratingComment");
     Route::post('passenger/trip/cms', 'Api\Passenger\RequestController@getCmsonTrip')->name("passenger.trip.cms");
     Route::post('passenger/on/ride/message', 'Api\Driver\MessageController@sendMessage')->name("passenger.trip.sendMessage");
     Route::post('passenger/on/ride/get/message', 'Api\Driver\MessageController@getMessage')->name("passenger.trip.getMessage");
     Route::post('passenger/split/search','Api\Passenger\RequestController@splitSearch')->name("passenger.splitSearch");
     Route::post('passenger/split/request','Api\Passenger\RequestController@requestSplit')->name("passenger.requestSplit");
     Route::post('passenger/split/response','Api\Passenger\RequestController@requestSplitAccptReject')->name("passenger.requestSplitAccptReject");
    
    });

});